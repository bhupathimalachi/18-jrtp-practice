import java.util.*;
public class Swapping{
	
	public void swapp(int x, int y){
		int temp;
		
		temp=x;
		x=y;
		y=temp;
		
		System.out.println("after swapping ");
		System.out.println("A=" + x);
		System.out.println("B=" + y);	
		
	}
	
	public void swapp(float m, float n){
		float temp;
		
		temp=m;
		m=n;
		n=temp;
		
		System.out.println("after swapping ");
		System.out.println("A=" + m);
		System.out.println("B=" + n);	
		
	}

	
	public static void main(String[] args){
		
		int a,b;
		float o,p;
		
		Scanner sc = new Scanner(System.in);
		System.out.println("enter A value ");
		a=sc.nextInt();
		System.out.println("enter B value");
		b=sc.nextInt();
		
		System.out.println("before swapping swapping ");
		System.out.println("A="+a);
		System.out.println("B="+b);
		
		Swapping s=new Swapping();
		s.swapp(a,b);
		
		
		System.out.println("enter o value ");
		o=sc.nextFloat();
		System.out.println("enter p value");
		p=sc.nextFloat();
		
		s.swapp(o,p);
		
		
	}
}